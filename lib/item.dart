class Item {
  final String usuario_nombre;
  final String usuario_email;
  final String usuario_password;
  Item({required this.usuario_nombre, required this.usuario_email, required this.usuario_password});

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(usuario_nombre: json['usuario_nombre'], usuario_password: json['usuario_password'], usuario_email: json['usuario_email']);
  }
}