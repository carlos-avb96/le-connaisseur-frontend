import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:login_register/screens/consejos_nutricionales.dart';
import 'package:login_register/screens/preguntas_frecuentes.dart';
//import 'package:login_register/screens/page5.dart';
import 'package:login_register/widgets/drawer_menu.dart';

import 'home_page.dart';

class Macronutrientes extends StatelessWidget{
  final int elusuarioid;
  const Macronutrientes(this.elusuarioid,{Key? key}) : super(key:key);
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent, // Color de fondo de la barra de la aplicación
        title: Text('App de calorias'), // Título de la barra de la aplicación
      ),
      // bottomNavigationBar: NavigationBar(
      //   backgroundColor: Colors.lightBlueAccent,// Barra de navegación en la parte inferior
      //   indicatorColor: Colors.amber,  // Índice de la pestaña seleccionada
      //   destinations: const <Widget>[ // Lista de destinos para las pestañas
      //     NavigationDestination(
      //       selectedIcon: Icon(Icons.home), // Icono seleccionado para la pestaña "Home"
      //       icon: Icon(Icons.home_outlined), // Icono para la pestaña "Home"
      //       label: 'Productos', // Etiqueta de la pestaña "Home"
      //     ),
      //     NavigationDestination(
      //       icon: Badge(child: Icon(Icons.account_balance_wallet_sharp)), // Icono para la pestaña "Notifications" con un badge
      //       label: 'Conteo', // Etiqueta de la pestaña "Notifications"
      //     ),
      //     NavigationDestination(
      //       icon: Badge( // Icono para la pestaña "Messages" con un badge
      //         label: Text('1'),
      //         child: Icon(Icons.messenger_sharp),
      //       ),
      //       label: 'Perfil', // Etiqueta de la pestaña "Messages"
      //     ),
      //   ],
      // ),
      drawer: DrawerMenu(this.elusuarioid),
      body: Container(
        margin: EdgeInsets.only(left: 20, top: 10, right: 20),
        child:Column(
          children: [
            Text('Bienvenido usuario: '+elusuarioid.toString()),
            Text('Los macronutrientes: ', style: TextStyle(color: Colors.lightBlue, fontSize: 15),),
            SizedBox(height: 10,),
            Expanded(child: Container(
              child:Column(
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(20.0),
                    child: Table(
                      border: TableBorder.all(color: Colors.black),
                      children: [
                        TableRow(
                            decoration: BoxDecoration(color: Colors.blueAccent[200]),
                            children: [
                              Text('Elmento', style: TextStyle(fontWeight: FontWeight.bold),),
                              Text('Descripcion', style: TextStyle(fontWeight: FontWeight.bold)),
                              Text('Utilidad', style: TextStyle(fontWeight: FontWeight.bold)),
                            ]),
                        TableRow(
                            decoration: BoxDecoration(color: Colors.grey[200]),
                        children: [
                          Text('Carbohidratos', style: TextStyle(fontWeight: FontWeight.bold),),
                          Text('Son el azucar, almidones, arroz y harinas.'),
                          Text('Dan energia '),
                        ]),
                        TableRow(children: [
                          Text('Grasas', style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('Son nutrientes\n que apoyan a\n los sistemas.'),
                          Text('Reserva de\n combustible.'),
                        ]),
                        TableRow(
                            decoration: BoxDecoration(color: Colors.grey[200]),children: [
                          Text('Proteinas', style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('Se encuentran en la carne, pescado, huevos.'),
                          Text('Reparan los\n musculos.'),
                        ])
                      ],
                    ),
                  ),
                  Text('Consulta a los expertos: ', style: TextStyle(color: Colors.lightBlue, fontSize: 15),),
                  Expanded(child: Container(

                    child: SizedBox(
                      height: 150.0,
                      child: ListView(
                          children: [
                            CarouselSlider(
                              items: [
                                Container(
                                  height: 150.0,
                                  margin: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    image: DecorationImage(
                                      image: NetworkImage("https://i.ytimg.com/vi/qtWPuNKafmk/maxresdefault.jpg"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 150.0,
                                  margin: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    image: DecorationImage(
                                      image: NetworkImage("https://www.directoriomedicoquito.com/picts/madeley%20chavez.jpg"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 150.0,
                                  margin: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    image: DecorationImage(
                                      image: NetworkImage("https://www.cronoshare.com/img/cuserphotos/4201104/lewtw1425pceo1y_big.jpg"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ],
                              options: CarouselOptions(
                                height: 150.0,
                                enlargeCenterPage: true,
                                autoPlay: true,
                                aspectRatio: 16 / 9,
                                autoPlayCurve: Curves.fastOutSlowIn,
                                enableInfiniteScroll: true,
                                autoPlayAnimationDuration: Duration(milliseconds: 800),
                                viewportFraction: 0.8,
                              ),
                            ),
                          ]
                      ),
                    ),
                  )),


                  SizedBox(height: 20,),
                ],
              ),
            ),
            ),
          ],
        ),
      ),
    );
  }
}