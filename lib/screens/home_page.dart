import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:login_register/screens/consejos_nutricionales.dart';
import 'package:login_register/screens/macronutrientes.dart';
import 'package:login_register/screens/preguntas_frecuentes.dart';
//import 'package:login_register/screens/page5.dart';
import 'package:login_register/widgets/drawer_menu.dart';

class PageStatefull extends StatelessWidget{
  final int elusuarioid;
  const PageStatefull(this.elusuarioid,{Key? key}) : super(key:key);
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent, // Color de fondo de la barra de la aplicación
        title: Text('App de calorias'), // Título de la barra de la aplicación
      ),
      // bottomNavigationBar: NavigationBar(
      //   backgroundColor: Colors.lightBlueAccent,// Barra de navegación en la parte inferior
      //   indicatorColor: Colors.amber,  // Índice de la pestaña seleccionada
      //   destinations: const <Widget>[ // Lista de destinos para las pestañas
      //     NavigationDestination(
      //       selectedIcon: Icon(Icons.home), // Icono seleccionado para la pestaña "Home"
      //       icon: Icon(Icons.home_outlined), // Icono para la pestaña "Home"
      //       label: 'Productos', // Etiqueta de la pestaña "Home"
      //     ),
      //     NavigationDestination(
      //       icon: Badge(child: Icon(Icons.account_balance_wallet_sharp)), // Icono para la pestaña "Notifications" con un badge
      //       label: 'Conteo', // Etiqueta de la pestaña "Notifications"
      //     ),
      //     NavigationDestination(
      //       icon: Badge( // Icono para la pestaña "Messages" con un badge
      //         label: Text('1'),
      //         child: Icon(Icons.messenger_sharp),
      //       ),
      //       label: 'Perfil', // Etiqueta de la pestaña "Messages"
      //     ),
      //   ],
      // ),
      drawer: DrawerMenu(this.elusuarioid),
      body: Container(
        margin: EdgeInsets.only(left: 20, top: 20),
        child:Column(
          children: [
           Text('Bienvenido usuario: '+elusuarioid.toString()),
            Expanded(child: Container(

              child: SizedBox(
                height: 150.0,
                child: ListView(
                    children: [
                      CarouselSlider(
                        items: [
                          Container(
                            height: 150.0,
                            margin: EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              image: DecorationImage(
                                image: NetworkImage("https://www.medicosecuador.com/images/pauta/anuncios/anunciodramariuxi.jpg"),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Container(
                            height: 150.0,
                            margin: EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              image: DecorationImage(
                                image: NetworkImage("https://i.masquemedicos.org/services/big-guayaquil-nutricionista-ana-maria-jimenez-20180612080637gi0a.jpg"),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Container(
                            height: 150.0,
                            margin: EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              image: DecorationImage(
                                image: NetworkImage("https://medicinaysaludpublica.blob.core.windows.net.optimalcdn.com/images/2022/06/10/mesa-de-trabajo-3portada-dieta-1-aa12fb83-focus-0-0-280-164.jpg"),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ],
                        options: CarouselOptions(
                          height: 150.0,
                          enlargeCenterPage: true,
                          autoPlay: true,
                          aspectRatio: 16 / 9,
                          autoPlayCurve: Curves.fastOutSlowIn,
                          enableInfiniteScroll: true,
                          autoPlayAnimationDuration: Duration(milliseconds: 800),
                          viewportFraction: 0.8,
                        ),
                      ),
                    ]
                ),
              ),
            )),
            Text('Informacion general: ', style: TextStyle(color: Colors.lightBlue, fontSize: 15),),
            Expanded(child: Container(
              child: ListView(
                children: const <Widget>[
                  ListTile(
                    leading: Icon(Icons.add_alert_sharp),
                    title: Text('Importancia del conteo de calorias'),
                    subtitle: Text('Ser consciente de las calorías que consumes puede facilitarte el lograr tus objetivos de peso y forma física.'),
                  ),
                  ListTile(
                    leading: Icon(Icons.accessibility_new_rounded),
                    title: Text('Como llevar un registro eficaz de las calorias?'),
                    subtitle: Text('Anote cada alimento que comas cada día, asi como la cantidad de la misma,  evitando usar palabras como “poco” o “sorbos”, sino pesos reales'),
                  ),
                  ListTile(
                    leading: Icon(Icons.account_box_sharp),
                    title: Text('Que son las calorias y que nos aportan?'),
                    subtitle: Text('Las calorías miden la energía que nos proporciona un alimento o una bebida a partir de los carbohidratos, las grasas, las proteínas y el alcohol que contienen.'),
                  ),
                ],
              ),
            )
            ),
            SizedBox(height: 40,),
          ],
        ),
      ),
    );
  }
}