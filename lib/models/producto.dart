class Producto {
  final String id;
  final String nombre;
  final dynamic calorias;
  final dynamic proteinas;
  final dynamic carbohidratos;
  final dynamic grasas;
  final String idUsuario;


  Producto({
    required this.id,
    required this.nombre,
    required this.calorias,
    required this.proteinas,
    required this.carbohidratos,
    required this.grasas,
    required this.idUsuario,
  });
  factory Producto.fromMap(Map<String, dynamic> map) {
    return Producto(
      id: map['producto_id'].toString(),
      nombre: map['producto_nombre']!=null?map['producto_nombre']:"",
      calorias: map['producto_calorias'],
      proteinas: map['producto_proteinas'],
      grasas: map['producto_grasas'],
      carbohidratos: map['producto_carbohidratos'],
      idUsuario: map['usuario_id'].toString(),
    );
  }

}