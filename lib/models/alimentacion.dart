
import 'package:login_register/models/producto.dart';
import 'package:login_register/models/usuario.dart';

class Alimentacion {
  final int? id;
  final String fecha;
  final Producto producto;
  final dynamic cantidad;
  final Usuario usuario;

  Alimentacion(
      {this.id,
        required this.fecha,
      required this.producto,
      required this.cantidad,
      required this.usuario});
  factory Alimentacion.fromMap(Map<String, dynamic> map,Usuario user,Producto product) {
    return Alimentacion(
      id: map['alimentacion_id'],
      fecha: map['alimentacion_fecha'],
      producto: product,
      cantidad: map['alimentacion_cantidad'],
      usuario: user
    );
  }

  @override
  String toString() {
    return 'Alimentacion{id: $id, fecha: $fecha, producto: $producto, cantidad: $cantidad, usuario: $usuario}';
  }
}
